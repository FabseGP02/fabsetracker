import React, { useState } from 'react';
import './App.css';

function App() {
  const [animeInfo, setAnimeInfo] = useState(null);
  const [animeTitle, setAnimeTitle] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const SERVER_IP = '127.0.0.1';

  const searchTitles = async () => {
    try {
      const response = await fetch(`http://${SERVER_IP}:5000/search/titles`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ animeTitle }),
      });
      const data = await response.json();
      console.log('Received data:', data);
      setSearchResults(data);
    } 
    catch (error) {
      console.error('An error occurred', error);
    }
  };

  const searchAnime = async (selectedTitle) => {
    console.log('Selected title:', selectedTitle);
    try {
      const response = await fetch(`http://${SERVER_IP}:5000/search/details`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ animeTitle: selectedTitle }),
      });
      const data = await response.json();
      setAnimeInfo(data);
    } 
    catch (error) {
      console.error('An error occurred', error);
    }
  };

  return (
    <div className="App">
      <h1>Fabseman Inc. hosted anime search</h1>
      <input
        type="text"
        placeholder="Enter anime title"
        value={animeTitle}
        onChange={(e) => setAnimeTitle(e.target.value)}
      />
      <button onClick={searchTitles}>Search</button>
      <ul>
        {console.log('searchResults:', searchResults)}
        {searchResults.map(result => (
          <li key={result.title} onClick={() => searchAnime(result)}>
            {result.title}
          </li>
        ))}
      </ul>
      {animeInfo && (
        <div className="anime-info">
          <h2>{animeInfo.title}</h2>
          <p>Type: {animeInfo.type}</p>
          <p>Season: {animeInfo.animeSeason.season} ({animeInfo.animeSeason.year})</p>
          <p>Episodes: {animeInfo.episodes}</p>
          <p>Status: {animeInfo.status}</p>
          <p>Genres: {animeInfo.tags.join(', ')}</p>
          {animeInfo.picture && <img src={animeInfo.picture} alt={animeInfo.title} />}
        </div>
      )}
    </div>
  );
}

export default App;
