const mongoose = require('mongoose');

const animeSchema = new mongoose.Schema({
  title: String,
  type: String,
  episodes: Number,
  status: String,
  picture: String,
  animeSeason: {
    season: String,
    year: Number,
  },
  tags: Array,
});

module.exports = mongoose.model('Anime', animeSchema);
