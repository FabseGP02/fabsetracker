const express = require('express');
const Anime = require('../models/Anime');
const router = express.Router();

router.post('/titles', async (req, res) => {
  const { animeTitle } = req.body;
  console.log('Searching for titles:', animeTitle);
  try {
    const regex = new RegExp(animeTitle, 'i');
    const animeTitles = await Anime.find({ title: regex }).distinct('title');
    console.log('Found titles:', animeTitles);
    res.json(animeTitles);
  } 
  catch (error) {
    console.error('Error:', error);
    res.status(500).json({ error: 'An error occurred' });
  }
});

router.post('/details', async (req, res) => {
  const { animeTitle } = req.body;
  console.log('Searching for details:', animeTitle);
  try {
    const anime = await Anime.findOne({ title: animeTitle });
    console.log('Found anime:', anime);
    res.json(anime);
  } 
  catch (error) {
    console.error('Error:', error);
    res.status(500).json({ error: 'An error occurred' });
  }
});

module.exports = router;
