const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const Anime = require('./models/Anime');
const searchRoute = require('./routes/search');

const app = express();

const PORT = 5000;
const SERVER_IP = '127.0.0.1';

app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost:27017/anime_db', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const animeJsonPath = path.join(__dirname, '../anime-database.json');

mongoose.connection.dropCollection('animes')
  .then(() => {
    console.log('Anime collection dropped.');
    fs.readFile(animeJsonPath, 'utf8', (err, data) => {
      if (err) {
        console.error('Error reading JSON file:', err);
        return;
      }
      const animeData = JSON.parse(data);
      Anime.insertMany(animeData.data)
        .then(() => {
          console.log('Data imported to MongoDB.');
          app.use('/search', searchRoute);
          app.listen(PORT, SERVER_IP, () => {
            console.log(`Server is running on ${SERVER_IP}:${PORT}`);
          });
        })
        .catch((error) => {
          console.error('Error importing data to MongoDB:', error);
        });
    });
  })
  .catch((error) => {
    console.error('Error dropping anime collection:', error);
  });
